# \<log-in\>

Identity and Access Proxy (IAP)

## Design
- APIs
    ```
    { PWA /
    Mobile App /       not yet login /
    Web App }          exisiting token (already login)
        Client +----------------------------+
                                            |
                                            v
                                        Authentication Howto +-------------------> User ID
                                                +
                                                |
                                                |
                                                |
                                  Introspection |
                                                v
                                    3rd Parties OAuth2 Servers
    ```
    - ~~Selected Ory Solution as a base.~~
    - not yet login -- should return to Client, and let client side to login.
    - Introspection -- verify the token fr. 3rd Parties OAuth2 Servers; if valid token, pass, otherwise return "not valid login".

- Inside Client: can be any different kind of projects and each project may have many users. 
    ```    
                                   +-----------------------------------------------------------------+
    Client +------+                |                          Login Module                           |
    flow          +--> Anonymouse  |                                                                 |
    (SPA)         |     screen(s)  |                             <-------------------------------+   |
                  |                |                                        Not Valid            |   |
                  |                |       logined                                               |   |
                  +--> protected +-------> by chk <----------------------------------------------+   |
                        screen(s)  | +---+ Token                  Save Token                     |   |
                            ^      | |yes  +   + no                                              |   |
                            +--------+     |   |                                                 |   |
    Sending out API                |       |   |                             +---> Facebook +----+   |
    asking for Token <---------------------+   |                             |                   |   |
                         Token     |           |                             +---> Google +------+   |
    not valid                      |           |                             |                   |   |
    login API(s) +-----------------------------+---> Login Module            +---> Own OAuth2 +--+   |
                                   |                   +                     |       Server      |   |
                                   |                   V                     |                   |   |
                                   |                 Login Screen            +---> .... +--------+   |
                                   |                   +                     |                       |
                                   |                   V                     |                       |
                                   |                 Authentication          |                       |
                                   |                 Methods: OAuth2 +-------+                       |
                                   |                                                                 |
                                   +-----------------------------------------------------------------+
    ```
    - Authentication Methods -- selected OAuth2. 

- Inside Server Side: can be any different kind of projects and each project may have many users. 
    ```
    +-------------------------------------+    +--------------------------------------------------+    +--------------------------+
    | Proj1                               |    | Proj2                                            |    | Proj3                    |
    |                                     |    |                                                  |    |                          |
    |  client1  client2  client3 ...      |    |  client1  client2  client3  client4  client5 ... |    |  client1  client2 ...    |
    |                                     |    |                                                  |    |                          |
    +-------------------------------------+    +--------------------------------------------------+    +--------------------------+
                                                    ||
                                                    ||
                                                    ||
                                                    vV
                        +------------------------------+--------------------------------------------------------+
                        |  Frappe: Development or Production                                                    |
                        |                                                                                       |
                        |   +-----------------------------------------------------------------+                 |
                        |   |                                                                 |                 |
                        |   |     App: Login (Depends on Proj. para. to select which table    |                 |
                        |   |                                                                 |                 |
                        |   +--------+--------------------+--------------------+----------+---+                 |
                        |            |                    |                    |          |                     |
                        +---------------------------------------------------------------------------------------+
                                    |                    |                    |          |
                                    v                    v                    v          |
                            +------+----+        +------+----+        +------+----+     | 
                            | Proj1 Tbl |        | Proj2 Tbl |        | Proj3 Tbl |     v
                            +-----------+        +-----------+        +-----------+
                            |   . . . . |        |   . . . . |        |   . . . . |
                            |   . . . . |        |   . . . . |        |   . . . . |    . . . .   Users Db
                            |   . . . . |        |   . . . . |        |   . . . . |
                            +-----------+        +-----------+        +-----------+

                                Each Project can have different user table design
    ```
    - Separating each project with one table as 
      - Pros
        - in case too many users, do not affect all projects, the db performance of one project with corresponding no. of user is acceptabe. 
        - Each project can have custom make user table structure.
      - Cons
        - Each new project requests to create a table process.
    - User Db -- if valid token, but new user corresponding fr. this token, add this new user. 

- Screenshots

![Screenshot](./demo/Screenshot.png)

## How to use it
- Detail code can reference fr. demo folder.
- import lib., eg. 
    ```
    npm install --save @shifang2013/log-in@x.y.z
    import '@shifang2013/log-in/log-in.js';
    ```
- add this element into your main page, eg.
    - Vue platform
        ```
        <log-in id="loginLib" ref="loginLib" 
            name="Your_App_Name" 
            description="Your App Description"
            fb-id="XXXXXXX"
            ggle-id="YYYYY"
            :login-flag="loginFlag"
            @login-flag-changed="loginFlag = $event.detail.value; test = $event;"
            on-after="getbackFrLogin">
        </log-in>
        ```
    - polymer 3.0 platform
        ```
        <log-in id="loginLib" 
            name="Your_App_Name" 
            description="Your App Description"
            fb-id="XXXXXXX"
            ggle-id="YYYYY"
            login-flag="{{loginFlag}}"`
            on-after="getbackFrLogin">
        </log-in>
        ```
- check login status, eg.
    - loginLib.goLogin()
        false -- non-login, auto. jump to login form and should skip this protected page
        true -- login, continue to the protected page.
        
- check login status, eg.
    - login-flag as
        - 0 = no login 
        - 1 = login
        - 2 = user tap account icon with no login

- get login user info.
    - log-in element will return a event -- on-after="getbackFrLogin", it calls 
        ```
        getbackFrLogin(loginData) {
            this.loginData = loginData.detail.value;
            ...
        }
        ```
    - this.loginData contains user info. as:
        ```
        { 
            token: id_token,
            picture: picture_url,
            email: Email string,
            last_name: Family Name string,
            first_name: Given Name string,
            user_id: user id string,
            social: 'ggle / fb'
        }
        ```

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) and npm (packaged with [Node.js](https://nodejs.org)) installed. Run `npm install` to install your element's dependencies, then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
