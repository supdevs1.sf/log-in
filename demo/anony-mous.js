import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `anony-mous`
 * Anonymous Page
 *
 * Author: 
 *  chungyan5@gmail.com
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class AnonyMous extends PolymerElement {

  static get template() {
    return html`
      <style>
      </style>
      
      <p>This is a Anonymous Page, the content is public.</p>
      
      <a name="main-demo" href="[[rootPath]]maindemo">Back Main Demo</a>
    `;
  }
  
  static get properties() {
    return {
    };
  }

}
window.customElements.define('anony-mous', AnonyMous);
