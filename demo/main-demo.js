import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';

import '@polymer/paper-button/paper-button.js';
import "@polymer/paper-dialog/paper-dialog.js";

import '../log-in.js';

/**
 * `main-demo`
 * Main Demo
 *
 * Author: 
 *  chungyan5@gmail.com
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class MainDemo extends PolymerElement {

  static get template() {
    return html`
      <style>
      </style>

      <div class="vertical-section-container centered">
        <h3>Basic log-in demo</h3>

        <p><a name="anony-mous" href="[[rootPath]]anonymous">Anonymous</a></p>
        <p><a name="pro-tect" href="[[rootPath]]protect">Protection Page</a></p>

        <template is="dom-if" if="[[_matchingLoginFlag(loginFlag, 1)]]">
          <paper-button raised on-tap="_logout">logout</paper-button>
        </template>

        <!-- Logout Procedure -->
        <paper-dialog id="logout_dialog">
            <div>
                <div></div>
                <div>Confirm Logout?</div>
                <div>
                    <paper-button raised on-tap="_confirm_logout">登出</paper-button>
                    <paper-button raised on-tap="_close_logout_dialog">關閉</paper-button>
                </div>
            </div>
        </paper-dialog>
  
      </div>
      
    `;
  }
  
  static get properties() {
    return {
    };
  }

  // does loginFlag match to provided state?
  //  loginFlag -- current loginFlag state
  //  state -- input a wanted state, see is it matching or not
  //    0 -- non login
  //    1 -- login
  // Remark:
  //  Using this object own property, which can works at init. stage, this property is 
  //    sync to loginObj.loginFlag for later on its loginFlag value.
  _matchingLoginFlag(loginFlag, state) {  
    return loginFlag===state;
  }
     
  _logout(page) {
    this.$.logout_dialog.open();
  }

  _close_logout_dialog() {
    this.$.logout_dialog.close();
  }
     
  _confirm_logout(){
    this.logObj.goLogout();
    this.$.logout_dialog.close();
  }

}
window.customElements.define('main-demo', MainDemo);
