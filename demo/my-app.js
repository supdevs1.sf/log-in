import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import {afterNextRender} from '@polymer/polymer/lib/utils/render-status.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';

import '@polymer/iron-selector/iron-selector.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set root path, eg. '/your_path/'
setRootPath('/demo/');

/**
 * `my-app`
 * My App
 *
 * Author: 
 *  chungyan5@gmail.com
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */

class MyApp extends PolymerElement {
  static get template() {
    return html`
      <style>
      </style>

      <log-in id="loginLib" 
        name="The Demo App" 
        description="This Application Description"
        fb-id="your_facebook_app_id"
        ggle-id="your_google_app_id"
        login-flag="{{loginFlag}}"
        on-after="getbackFrLogin">
      </log-in>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <main-demo name="maindemo" log-obj="[[logObj]]" login-flag="[[loginFlag]]""></main-demo>
            <anony-mous name="anonymous"></anony-mous>
            <pro-tect name="protect"></pro-tect>
            <my-view404 name="view404"></my-view404>
          </iron-pages>

        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,

      /**
       * login succss return page
       *  init. as Main Demo Page
       */
      sucRoute: {
        type: String,
        value: "maindemo"
      },

      /**
       * login failure return page
       */
      faiRoute: {
        type: String,
      },

      /**
       * Login Object
       */
      logObj: {
        type: Object,
      },

    };
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  constructor() {
    super();

    // When possible, use afterNextRender to defer non-critical
    // work until after first paint.
    afterNextRender(this, function() {
      this.logObj = this.$.loginLib;
    });
    
  }

  _routePageChanged(page) {

    // Show the corresponding page according to the route.
    //
    // If no page was found in the route data, page will be an empty string.
    // Show 'maindemo' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'maindemo';
    } else if (['maindemo', 'anonymous'].indexOf(page) !== -1) {
      this.page = page;
    } else if (['protect'].indexOf(page) !== -1) {

        if (this.$.loginLib.goLogin()) {
          this.page = page;
          //} else {
          //  page = 'maindemo';
        } else {

          // this demo application save "page" string after login.
          //  this login procedure be kept at _routePageChanged, this solution can avoid user directly typing the route to enter the protected page.
          this.sucRoute = 'protect';
          this.faiRoute = '';
        }
    } else {
      this.page = 'view404';
    }
  }

  // callback this function after login
  getbackFrLogin(e) {  

    // after success login
    this.page = this.sucRoute;
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'maindemo':
        import('./main-demo.js');
        break;
      case 'anonymous':
        import('./anony-mous.js');
        break;
      case 'protect':
        import('./pro-tect.js');
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }

}

window.customElements.define('my-app', MyApp);
