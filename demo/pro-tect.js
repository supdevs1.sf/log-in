import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `pro-tect`
 * Protected Page
 *
 * Author: 
 *  chungyan5@gmail.com
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class ProTect extends PolymerElement {

  static get template() {
    return html`
      <style>
      </style>
      


      <p>This is a Protected Page, the content is private and seen after login.</p>
      
      <a name="main-demo" href="[[rootPath]]maindemo">Back Main Demo</a>
    `;
  }
  
  static get properties() {
    return {
    };
  }

  /*connectedCallback() {
    super.connectedCallback();
  }*/

  // Description:
  //  get loginFlag, if false which is non-login, auto. jump to login form
  getLoginFlag() {
    if (this.loginFlag) {
      console.log("true ");
      return true;
    } else {
      console.log("false ");
      return false;
    }
  }

}
window.customElements.define('pro-tect', ProTect);
