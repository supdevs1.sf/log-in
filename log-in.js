import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/* In an existing module / web component */
import './vaadin-login/vaadin-login-overlay.js';

/**
 * `log-in`
 * Identity and Access Proxy (IAP)
 * 
 * Parameters:
 *  after login
 * 
 *  loginFlag  ~~getLoginFlag()~~
 *    Descrition:
 *      Do not use a method solution, because 3rd parties lib. call this method, i.e. 3rd parties is active; 
 *      Using a variable solution, login lib. can active to update 3rd parties lib., i.e. 3rd parties lib. is passive.
 *    Values Meaning
 *      0 = no login
 *      1 = login
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class LogIn extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }

      </style>
      
      <vaadin-login-overlay id="loginFrm" 
        fb-id="[[fbId]]" ggle-id="[[ggleId]]" 
        on-user-changed="_fb_login_succ" 
        on-google-signin-success="_ggle_login_succ"
        description="[[description]]">

        <h1 slot="title" class="customTitle">
          <iron-icon icon="vaadin:vaadin-h" class="logo"></iron-icon>
          [[name]]
        </h1>

      </vaadin-login-overlay>
    `;
  }
  static get properties() {
    return {

      // Value meaning
      //  0 = no login 
      //  1 = login
      //  2 = user tap account icon with no login
      loginFlag: {
        type: Number,
        value: 0,
        reflectToAttribute: true,
        notify: true
      },

      // Facebook App ID
      fbId: {
        type: String,
        reflectToAttribute: true
      },

      // Google App ID
      ggleId: {
        type: String,
        reflectToAttribute: true
      },

      /**
       * Defines the application name
       */
      name: {
        type: String,
        value: 'My App',
      },

      /**
       * Defines the application description
       */
      description: {
        type: String,
        value: 'My App Description',
      },

      /**
       * Previous route before login
       */
      preRoute: {
        type: String,
        value: '/',
      },

    };
  }

  /*connectedCallback() {
    super.connectedCallback();
    console.log("a10 ");
  }*/

  // does loginFlag match to provided state?
  //  loginFlag -- current loginFlag state
  //  state -- input a wanted state, see is it matching or not
  //    0 -- non login
  //    1 -- login
  // Remark:
  //  This loginFlag may not init.(as undefine) in a right time due to polymer all elements init. seq.
  matchingLoginFlag(loginFlag, state) {  
    return loginFlag===state;
  }

  // Description: goto login, if already logined, skip
  //  return
  //    false -- non-login, auto. jump to login form and should skip this protected page
  //    true -- login, continue to the protected page.
  goLogin() {
    if (this.loginFlag==1) return true;
    else {
      this.$.loginFrm.opened = true;
      return false;
    }
  }

  // Return to here after success login fr. OAuth Server.
  // 
  _fb_login_succ(e) {
    //console.log("fb login object ", e.detail.value);
    this._onloginSucc(
      { 
        token: e.detail.value.token,
        picture: e.detail.value.picture.data.url,
        email: e.detail.value.email,
        last_name: e.detail.value.last_name,
        first_name: e.detail.value.first_name,
        user_id: e.detail.value.id,
        social: 'fb'
      }
    );
  }

  // Google Success Login
  _ggle_login_succ(e) {

    // Reference: [Google Sign-In eg.](https://codepen.io/meyu/pen/yEMLwY)
    var googleUser = gapi.auth2.getAuthInstance()['currentUser'].get();
    //console.log("after ggle login e ", e);
    //console.log("after ggle login gapi  ", googleUser);
    //console.log("token  ", googleUser.getAuthResponse().id_token );
    //console.log("image  ", googleUser.getBasicProfile().getImageUrl() );
    //console.log("email  ", googleUser.getBasicProfile().getEmail() );
    //console.log("Surname  ", googleUser.getBasicProfile().getFamilyName() );
    //console.log("first_name  ", googleUser.getBasicProfile().getGivenName() );
    //console.log("user_id  ", googleUser.getBasicProfile().getId() );
    this._onloginSucc(
      { 
        token: googleUser.getAuthResponse().id_token,
        picture: googleUser.getBasicProfile().getImageUrl(),
        email: googleUser.getBasicProfile().getEmail(),
        last_name: googleUser.getBasicProfile().getFamilyName(),
        first_name: googleUser.getBasicProfile().getGivenName(),
        user_id: googleUser.getBasicProfile().getId(),
        social: 'ggle'
      }
    );
    
  }

  _onloginSucc(loginData) {
    
    this.loginFlag = 1;

    // send out data for backend user db
    

    // fire out a event for login Successful with "Login Data" 
    this.dispatchEvent(
      new CustomEvent('after', 
        { detail: {value: loginData}
        }
      )
    );

    this.$.loginFrm.opened = false;
  }

  goLogout() {
      this.$.loginFrm.logout();
      this.loginFlag = 0;
  }
     
  close_dialog () {
    this.$$('#logout_dialog').close();
  }

}

window.customElements.define('log-in', LogIn);
