import './vaadin-login-form-wrapper.js';
import '@vaadin/vaadin-text-field/src/vaadin-text-field.js';
import '@vaadin/vaadin-text-field/src/vaadin-password-field.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import { LoginMixin } from './vaadin-login-mixin.js';
import { ElementMixin } from '@vaadin/vaadin-element-mixin/vaadin-element-mixin.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { PolymerElement } from '@polymer/polymer/polymer-element.js';

import '@shifang2013/paper-facebook-login/paper-facebook-login.js';
import '@google-web-components/google-signin/google-signin.js';

/**
 * `<vaadin-login-form>` is a Web Component providing an easy way to require users
 * to log in into an application. Note that component has no shadowRoot.
 *
 * ```
 * <vaadin-login-form></vaadin-login-form>
 * ```
 *
 * Component has to be accessible from the `document` layer in order to allow password managers to work properly with form values.
 * Using `<vaadin-login-overlay>` allows to always attach the component to the document body.
 *
 * ### Styling
 *
 * The component doesn't have a shadowRoot, so the html form and input fields can be styled in an upper layer. To style
 * `vaadin-login-form-wrapper` check its documentation.
 *
 * See examples of setting the content into slots in the live demos.
 * @memberof Vaadin
 * @mixes Vaadin.ElementMixin
 * @mixes Vaadin.ThemableMixin
 * @mixes Vaadin.Login.LoginMixin
 * @demo demo/index.html
 */

/*
 * Login Form Layout 
 *  Reference: [w3schools.com - How TO - Social Login Form](https://www.w3schools.com/howto/howto_css_social_login.asp)
 */

class LoginFormElement extends LoginMixin(ElementMixin(ThemableMixin(PolymerElement))) {
  static get template() {
    return html`
    <style>
      [part="vaadin-login-native-form"] * {
        width: 100%;
      }

      body {
        font-family: Arial, Helvetica, sans-serif;
      }
      
      * {
        box-sizing: border-box;
      }
      
      /* style the container */
      .container {
        position: relative;
        border-radius: 5px;
        background-color: #f2f2f2;
        padding: 20px 0 30px 0;
      } 
      
      /* style inputs and link buttons */
      input,
      .btn {
        width: 100%;
        padding: 12px;
        border: none;
        border-radius: 4px;
        margin: 5px 0;
        opacity: 0.85;
        display: inline-block;
        font-size: 17px;
        line-height: 20px;
        text-decoration: none; /* remove underline from anchors */
      }
      
      input:hover,
      .btn:hover {
        opacity: 1;
      }
      
      /* add appropriate colors to fb, twitter and google buttons */
      .fb {
        background-color: #3B5998;
        color: white;
      }
      
      .twitter {
        background-color: #55ACEE;
        color: white;
      }
      
      .google {
        background-color: #dd4b39;
        color: white;
      }
      
      /* style the submit button */
      input[type=submit] {
        background-color: #4CAF50;
        color: white;
        cursor: pointer;
      }
      
      input[type=submit]:hover {
        background-color: #45a049;
      }
      
      /* Two-column layout */
      .col {
        float: left;
        width: 100%;
        margin: auto;
        padding: 0 50px;
        margin-top: 0;
      }
      
      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }
      
      /* vertical line 
        position: absolute;
        left: 50%;
        transform: translate(-50%);
        border: 2px solid #ddd;
        height: 175px;
      */
      .vl {
        display: none;
      }
      
      /* text inside the vertical line */
      .vl-innertext {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
        background-color: #f1f1f1;
        border: 1px solid #ccc;
        border-radius: 50%;
        padding: 8px 10px;
      }
      
      /* hide some text on medium and large screens 
      display: none;
      */
      .hide-md-lg {
        display: block;
        text-align: center;
    }
      
      /* bottom container */
      .bottom-container {
        text-align: center;
        background-color: #666;
        border-radius: 0px 0px 4px 4px;
      }
      
      /* Responsive layout - when the screen is less than 650px wide, make the two columns stack on top of each other instead of next to each other */
      /* 
      @media screen and (max-width: 650px) {
      .col {
          width: 100%;
          margin-top: 0;
        }
        /* hide the vertical line */
        .vl {
          display: none;
        }
        /* show the hidden text on small screens */
        .hide-md-lg {
          display: block;
          text-align: center;
        }
      }
      */
   </style>

    <div class="container">
    <form action="/action_page.php">
      <div class="row">
        <h2 style="text-align:center">Login with Social Media
          <!--  or Manually --> </h2>
        <div class="vl">
          <span class="vl-innertext">or</span>
        </div>
  
        <div class="col">

          <paper-facebook-login class="btn" id="fbId"
            app-id="[[fbId]]" 
            on-user-changed="_retargetEvent">
          </paper-facebook-login>
          <google-signin class="btn" brand="google" width="wide" height="tall" id="ggleId" 
            client-id="[[ggleId]]"
            on-google-signin-success="_retargetEvent">
          </google-signin>
           
          <!--  <a href="#" class="twitter btn">
            <i class="fa fa-twitter fa-fw"></i> Login with Twitter
          </a> -->
        </div>
  
        <!-- <div class="col">
          <div class="hide-md-lg">
            <p>Or sign in manually:</p>
          </div>
  
          <input type="text" name="username" placeholder="Username" required>
          <input type="password" name="password" placeholder="Password" required>
          <input type="submit" value="Login">
        </div> -->
        
      </div>
    </form>
  </div>
  
  <!-- <div class="bottom-container">
    <div class="row">
      <div class="col">
        <a href="#" style="color:white" class="btn">Sign up</a>
      </div>
      <div class="col">
        <a href="#" style="color:white" class="btn">Forgot password?</a>
      </div>
    </div>
  </div>-->

    `;
  }

  static get is() {
    return 'vaadin-login-form';
  }
  static get version() {
    return '1.0.1';
  }

  static get properties() {
    return {

      /**
       * Defines the theme of the element.
       * The value is propagated to vaadin-login-form-wrapper element.
       */
      theme: {
        type: String
      },

      // Facebook App ID
      fbId: {
        type: String,
        reflectToAttribute: true
      },

      // Google App ID
      ggleId: {
        type: String,
        reflectToAttribute: true
      },

    };
  }

  connectedCallback() {
    super.connectedCallback();
    this._handleInputKeydown = this._handleInputKeydown.bind(this);
  }

  _attachDom(dom) {
    this.appendChild(dom);
  }

  static get observers() {
    return [
      '_errorChanged(error)'
    ];
  }

  _errorChanged() {
    if (this.error && !this._preventAutoEnable) {
      this.disabled = false;
    }
  }

  submit() {
    if (this.disabled || !(this.__isValid(this.$.vaadinLoginUsername) && this.__isValid(this.$.vaadinLoginPassword))) {
      return;
    }

    this.error = false;
    this.disabled = true;

    const loginEventDetails = {
      bubbles: true,
      cancelable: true,
      detail: {
        username: this.$.vaadinLoginUsername.value,
        password: this.$.vaadinLoginPassword.value
      }
    };

    const firedEvent = this.dispatchEvent(new CustomEvent('login', loginEventDetails));
    if (this.action && firedEvent) {
      this.querySelector('[part="vaadin-login-native-form"]').submit();
    }
  }

  __isValid(input) {
    return (input.validate && input.validate()) || (input.checkValidity && input.checkValidity());
  }

  _isEnterKey(e) {
    return e.key === 'Enter' || e.keyCode === 13;
  }

  _handleInputKeydown(e) {
    if (this._isEnterKey(e)) {
      const {currentTarget: inputActive} = e;
      const nextInput = inputActive.id === 'vaadinLoginUsername'
        ? this.$.vaadinLoginPassword : this.$.vaadinLoginUsername;
      if (this.__isValid(inputActive)) {
        if (this.__isValid(nextInput)) {
          this.submit();
        } else {
          nextInput.focus();
        }
      }
    }
  }

  _handleInputKeyup(e) {
    const isTab = e.key === 'Tab' || e.keyCode === 9;
    const input = e.currentTarget;
    if (isTab && input && input.select) {
      input.select();
      // iOS 9 workaround: https://stackoverflow.com/a/7436574
      setTimeout(() => input.setSelectionRange(0, 9999));
    }
  }

  logout() {
    //console.log("logout ", this.$.fbId.user);
    if (typeof this.$.fbId.user !== "undefined")    // facebook return err. during logout() if not yet login before
      this.$.fbId.logout();
    this.$.ggleId.signOut();
  }

}

customElements.define(LoginFormElement.is, LoginFormElement);

export { LoginFormElement };
